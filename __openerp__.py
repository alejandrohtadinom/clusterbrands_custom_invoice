# -*- coding: utf-8 -*-
# Copyright 2018 Alejandro H Tadino M <https://alejandrohtadinom.github.io>
# License LGPL-3.0 or later (http://www.gnu.org/licenses/gpl.html).

{
    'name': "ClusterBrands Custom Report",

    'summary': """ClusterBrands reporte en formato media carta""",

    'description': """Este modulo crea una nueva entrada en el menú de impresión de reportes y facturas para imprimir las facturas en formato media carta""",

    'author': "José J Perez M & Alejandro H Tadino M",
    'website': "http://alejandrohtadinom.github.io",
    'support': 'alejandrohtadinom@gmail.com',
    'license': 'GPL-3',

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Accounting',
    'version': '1.2',

    # any module necessary for this one to work correctly
    'depends': ['base_setup', 'product', 'analytic', 'board', 'edi', 'report', 'account'],

    # always loaded
    'data': [
        'report/cluster_invoice.xml',
        'report/cluster_layout.xml',
        #'report/cluster_footer.xml',
        'report/report.xml',
    ],

    'css': [
        'static/src/css/style.css',
    ],

    # only loaded in demonstration mode
    # 'demo': [
    #     'data/demo.xml',
    # ],

    'installable': True,
}
